package gestionAcademica;
import java.util.List;

/**
 * Clase que representa una asignatura con su nombre, código y lista de cursos.
 */
public class Asignatura {
    private String nombre;
    private String codigo;
    private List<String> cursos;

    /**
     * Constructor que inicializa los atributos de la clase.
     * 
     * @param nombre Nombre de la asignatura.
     * @param codigo Código de la asignatura.
     * @param cursos Lista de cursos de la asignatura.
     */
    public Asignatura(String nombre, String codigo, List<String> cursos) {
        this.nombre = nombre;
        this.codigo = codigo;
        this.cursos = cursos;
    }

    /**
     * Devuelve el nombre de la asignatura.
     * 
     * @return Nombre de la asignatura.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Establece el nombre de la asignatura.
     * 
     * @param nombre Nombre de la asignatura.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Devuelve el código de la asignatura.
     * 
     * @return Código de la asignatura.
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Establece el código de la asignatura.
     * 
     * @param codigo Código de la asignatura.
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Devuelve la lista de cursos de la asignatura.
     * 
     * @return Lista de cursos de la asignatura.
     */
    public List<String> getCursos() {
        return cursos;
    }

    /**
     * Establece la lista de cursos de la asignatura.
     * 
     * @param cursos Lista de cursos de la asignatura.
     */
    public void setCursos(List<String> cursos) {
        this.cursos = cursos;
    }
}

