/**
 * Este paquete contiene las clases necesarias para gestionar la información académica de una institución educativa.
 * Las clases incluidas en este paquete son:
 * <ul>
 * <li>{@link Alumnos}: Contiene el DNI, el nombre, los apellidos y la fecha de nacimiento de un alumno.</li>
 * <li>{@link Asignaturas}: Contiene el código de asignatura, el nombre y el curso en el que se imparte.</li>
 * <li>{@link Curso}: Contiene el nombre del curso y del ciclo.</li>
 * <li>{@link Notas}: Contiene la evaluación, la asignatura y el curso.</li>
 * </ul>
 * Además, este paquete contiene una clase con método Main llamada {@link AppGestionAcademica}, que proporciona los siguientes métodos:
 * <ul>
 * <li>{@link AppGestionAcademica#ListarAlumnos()}: Permite obtener una lista de todos los alumnos registrados.</li>
 * <li>{@link AppGestionAcademica#ListarNotasAsignatura(String)}: Permite obtener una lista de las notas de una asignatura determinada.</li>
 * <li>{@link AppGestionAcademica#ListarNotasEvaluacion(String)}: Permite obtener una lista de las notas de una evaluación determinada.</li>
 * <li>{@link AppGestionAcademica#ImprimirBoletinNotasAlumno(String)}: Permite imprimir el boletín de notas de un alumno determinado.</li>
 * </ul>
 * @author Alvaro Fernandez Lopez
 */
package gestionAcademica;
